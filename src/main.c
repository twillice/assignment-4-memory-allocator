#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define MAP_ANONYMOUS 0x20

static struct block_header *get_block(void *mem) {
    return (struct block_header *) (mem - offsetof(struct block_header, contents));
}

void test_allocation() {
    printf("\n[*] Testing allocation...\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem = _malloc(0);
    assert(mem != NULL && "ALLOCATION FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("[+] ALLOCATION SUCCESS\n");
}

void test_free_block() {
    printf("\n[*] Testing free block...\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem = _malloc(100);
    assert(mem != NULL);
    _free(mem);
    assert(get_block(mem)->is_free && "FREE BLOCK FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("[+] FREE BLOCK SUCCESS\n");
}

void test_free_blocks() {
    printf("\n[*] Testing free two blocks...\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem1 = _malloc(100);
    void *mem2 = _malloc(500);
    assert(mem1 != NULL && mem2 != NULL);
    _free(mem1);
    _free(mem2);
    assert(get_block(mem1)->is_free && get_block(mem2)->is_free && "FREE BLOCKS FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("[+] FREE BLOCKS SUCCESS\n");
}

void test_mem_out_region_extended() {
    printf("\n[*] Testing region extension...\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem1 = _malloc(4096);
    void *mem2 = _malloc(4096);
    assert(mem1 != NULL && mem2 != NULL);
    assert((void *) get_block(mem1) + offsetof(struct block_header, contents) + 4096 == (void*) get_block(mem2) &&
                   "REGION EXTENSION FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("[+] REGION EXTENSION SUCCESS\n");
}

void test_mem_out_region_created() {
    printf("\n[*] Testing region created...\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mmap_addr = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    assert(mmap_addr != MAP_FAILED);
    void *mem1 = _malloc(128);
    void *mem2 = _malloc(0);
    assert(mem1 != NULL && mem2 != NULL);
    assert(get_block(mem2) != heap + REGION_MIN_SIZE && "REGION CREATION FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("[+] REGION CREATION SUCCESS\n");
}

int main() {
    test_allocation();
    test_free_block();
    test_free_blocks();
    test_mem_out_region_extended();
    test_mem_out_region_created();
    return 0;
}
